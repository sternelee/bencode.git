## bencode-worker

使用 `bencode` + `web worker` 将bt文件转成磁力链接

```
import bencode from '@sternelee/bencode'

const file = document.getElementById('file')
file?.addEventListener('change', (evt: any) => {
    let file = evt.target.files[0]
    console.log(file)
    const reader = new FileReader()
    reader.readAsArrayBuffer(file)
    reader.onload = async function (e: any) {
        const result = await bencode(e.target.result)
        console.log(result)
    }
})
```
